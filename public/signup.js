function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var signinEmail = document.getElementById('signinemail');
    var signinPassword = document.getElementById('signinpsw');
    console.log(btnSignUp);
    console.log(txtEmail);
    console.log(txtPassword);


    btnLogin.addEventListener('click', function() {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()" and clean input field
        var email = signinEmail.value;
        var password = signinPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(error){
            window.location.href = "hall.html";
            notifyMe();
        }).catch(function(error){
            email = '';
            password = '';
        });
    });

    btnGoogle.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result){
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "hall.html";
        }).catch(function(error){
            //create_alert("error", error.message);
        });
    });

    btnSignUp.addEventListener('click', function() {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        console.log('init');
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(error){
            
            email = '';
            password = '';
        }).catch(function(error){
            console.log('signup fail');
            console.log(error);
            //create_alert("error", error.message);
            email = '';
            password = '';
        });
        document.getElementById('id01').style.display='none';
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};

function notifyMe(data) {
    // first, cirtify the browser supports the Notification
    console.log('data: ' + data);
    if (!("Notification" in window)) {
      alert("the browser not support the Notification");
    }
  
    // recheck whether the user has authorized to execute the Notification
    else if (Notification.permission === "granted") {
      // add the Notification if it is authorized
      var notification = new Notification('login success');
    }
  
    // otherwise, we need to ask the user whether to open the authorization
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        // add a Notification if the user agree
        if (permission === "granted") {
          var notification = new Notification('login success');
        }
      });
    }
  
    // if the user still doesn't agree to authorize the execution of Notification
    // we should better carry on a proper deal to avoid disturbign the user
}
