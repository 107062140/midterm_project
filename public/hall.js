function init() {
    var user_email = '';
    var context, contextf;
    var node;
    
    document.getElementById('add').onclick = function(){
        console.log('add click');

        context = document.getElementById('inputtext').value;
        node = context;
    }

    document.getElementById('addf').onclick = function(){
        console.log('addf click');
        contextf = document.getElementById('friend').value;
        var topush = {
            friend: contextf + '@gmail.com'
        }
        firebase.database().ref(node).push(topush);
    }

    const user = firebase.auth().currentUser;
    const credential = firebase.auth.EmailAuthProvider.credential(
        user.email, 
        userProvidedPassword
    );

    firebase.auth().signInWithCredential(credential).catch(function(user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='dropdown-item' href='#'>" + user.email + "</a><a class='dropdown-item' id='logout-btn' href='#'>Logout</a>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function() {
                firebase.auth().signOut()
                    .then(function() {
                        alert('Sign Out!')
                        window.location.href = 'index.html';
                    })
                    .catch(function(error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var str_before_username_master = "<div class='container' style='height:80px; width:100%; border:2px solid #dedede; background-color: #f1f1f1; border-radius: 5px; padding: 10px; margin: 10px 0;'>";
            var str_before_username_guest = "<div class='container' style='height:80px; width:100%; border:2px solid #dedede; background-color: #f1f1f1; border-radius: 5px; padding: 10px; margin: 10px 0; border-color: #ccc; background-color: #ddd;'>";
            var str_of_avatar_master = "<img src='master.png' alt='Avatar' style='float: right; max-width: 60px; margin-left: 20px; margin-right:0; border-radius: 50%'>";
            var str_of_avatar_guest = "<img src='guest.png' alt='Avatar' style='float: left; max-width: 60px; width: 100%; margin-right: 20px; border-radius: 50%;'>";
            var str_after_content = "</div>\n";

            var testpostsRef = firebase.database().ref();

            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            var count = 0;
            var ingroup = false;
            console.log('node: ' + node);
            
            testpostsRef.child(node).once('value')
                   .then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        // console.log('childSnapshot.val(): ' + snapshot.child(node));
                        if(snapshot.child(node)){
                            console.log('childSnapshot.val().status: ' + childSnapshot.val().status);
                            if(childSnapshot.val().status == 'master' && user_email == childSnapshot.val().email){
                                console.log('childSnapshot.val().friend: ' + childSnapshot.val().friend);
                                var topush = {
                                    email: user_email,
                                    data: escapeHtml(post_txt.value),
                                    status: 'master'
                                }
                                console.log('topush: ' + topush);
                                firebase.database().ref(node).push(topush);
                                ingroup = true;
                                return true;
                            }
                            else if(user_email==childSnapshot.val().friend){
                                console.log('childSnapshot.val().friend: ' + childSnapshot.val().friend);
                                var topush = {
                                    email: user_email,
                                    data: escapeHtml(post_txt.value)
                                }
                                console.log('topush: ' + topush);
                                firebase.database().ref(node).push(topush);
                                ingroup = true;
                                return true;
                            }
                            else{
                                console.log('not in the group');
                                count = count + 1;
                            }
                        }
                    })
                    if(!ingroup && count==0){
                        console.log('count: ' + count);
                        var topush = {
                            email: user_email,
                            data: escapeHtml(post_txt.value),
                            status: 'master'
                        }
                        console.log('topush: ' + topush);
                        firebase.database().ref(node).push(topush);
                    }
                    console.log('snapshot: ' + snapshot);
                
                    snapshot.forEach(function(childSnapshot){
                        if(childSnapshot.val().email!=undefined){
                            if(childSnapshot.val().status == 'master'){
                                total_post[total_post.length] = str_before_username_master + str_of_avatar_master + "</strong>" + childSnapshot.val().data + str_after_content;
                            }
                            else{
                                total_post[total_post.length] = str_before_username_guest + str_of_avatar_guest + "</strong>" + childSnapshot.val().data + str_after_content;
                            }
                            first_count = first_count + 1;
                            console.log('childSnapshot.val().data: ' + childSnapshot.val().data)
                        }
                    })

                    testpostsRef.child(node).on('child_added', function(data) {
                        if(data.val().friend == undefined){
                            second_count = second_count + 1;
                        }
                        console.log('first_count: ' + first_count);
                        console.log('second_count: ' + second_count);
    
                        if (second_count > first_count) {
                            var childData = data.val();
                            if(childData.status == 'master'){
                                total_post[total_post.length] = str_before_username_master + str_of_avatar_master + "</strong>" + childData.data + str_after_content;
                            }
                            else{
                                total_post[total_post.length] = str_before_username_guest + str_of_avatar_guest + "</strong>" + childData.data + str_after_content;
                            }
                            document.getElementById('post_list').innerHTML = total_post.join('');
                            console.log('length: ' + total_post.length);
                            first_count = total_post.length;
                            
                        }
                    });
                })
        }
    });
}

window.onload = function() {
    init();
}

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
  }

