# Software Studio 2020 Spring Midterm Project

https://drive.google.com/file/d/1JpduNnYeCHCDojQCPN49SThXcKy-8Uwd/view?usp=sharing

## Topic
* Project Name : Midterm Project_Chatroom
* Key functions (add/delete)
  Add friend and chat with others 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-fd958.web.app

# Components Description : 
1. Sign in : Can only sign up with the account that appends @gmail.com or just sign in with Google
2. Chat room : You can't create the room name has existed, otherwise, your room can't be created. Every member in the room can add friends to the room.
3. Chrome notification : When you login success, the chrome notification will pop up.

# Other Functions Description : 
No

## Security Report (Optional):
```javascript
function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
 }
```

### Reference:
1. https://www.w3schools.com/howto/howto_css_social_login.asp
2. https://www.w3schools.com/HOWTO/howto_css_chat.asp
3. https://stackoverflow.com/questions/22279231/using-js-jquery-how-can-i-unescape-html-and-put-quotes-back-in-the-str
4. Lab06
